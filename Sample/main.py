#!/usr/bin/env python3
# coding = utf-8

import sys,os
filePath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(filePath,'..\\'))
from AdvancedLogging import AdLogging
import Test1

if __name__ == '__main__':
    logger = AdLogging.AdLogging.getLogger(__name__)
    logger.Info("Info OK")
    logger.Debug("Debug OK")
    logger.Trace("Trace OK")
    logger.Warn("Warning OK")
    logger.Error("Error OK")
    logger.Critical("Critical OK")
    Test1.getTest()
"""
Author: Jeff Tu
Date: 2021-11-22 13:32:25
LastEditTime: 2021-11-25 09:40:02
LastEditors: Jeff Tu
Description: AdLogging Module
license LGPL 3.1
"""
#!/usr/bin/env python3
# coding = utf-8
#!/usr/bin/env python3
# coding = utf-8
import logging
from logging import handlers
import re
import colorlog
from typing import Optional
from enum import IntEnum

#handler = None
logger = None
_ModuleName: Optional[str] = None
_FileHandler_Args: Optional[str] = None
_SetFileName_flag:bool = False

class FileHandler_Args_Info(IntEnum):
    WhenAndInterval = 0
    FileName = 1

class AdLogging:
    @classmethod
    def getLogger(cls, ModuleName: Optional[str]=None,FileHandler_Args: Optional[str] = None):
        """獲取AdLogging

        Args:
            ModuleName (str | None, optional): 設定logger模組名稱. Defaults to None.
            FileHandler_Args (str | None, optional): 設定模組檔案名稱格式和切檔間隔.格式為[(When)(Interval)]Filename Defaults to None.

        Returns:
            AdLogging: 回傳完成的logger
        """
        global _ModuleName
        if ModuleName != '__main__' and ModuleName != None:
            _ModuleName = re.findall(r'[\.]?([\w]+)$', ModuleName, re.I)[0]
        else:
            _ModuleName = None
        global _FileHandler_Args
        if _FileHandler_Args == None and FileHandler_Args != None:
            _FileHandler_Args = FileHandler_Args
        return AdLogging()
    def __init__(self):
        global logger
        global _ModuleName
        self.__ModuleName = _ModuleName
        global _SetFileName_flag
        global _FileHandler_Args
        if _FileHandler_Args != None and _SetFileName_flag == False :
            FileHandler_Args_Data = re.findall(r"^(?:\[([SsMmHhDd)][1-9][0-9]*|[Ww][0-6]-[Ww][1-6]|[Mm][Ii][Dd][Nn][Ii][Gg][Hh][Tt][1-9][0-9]*)\])([A-Za-z0-9\.\%_\-]+)$",_FileHandler_Args)
            if len(FileHandler_Args_Data) != 1:
                raise ValueError("Filename formater error")
            else:
                file_handler: handlers.TimedRotatingFileHandler
                FileHandler_Args = FileHandler_Args_Data[0]
                if  FileHandler_Args[FileHandler_Args_Info.WhenAndInterval][0].upper() =='W':
                    when_string =FileHandler_Args[FileHandler_Args_Info.WhenAndInterval]
                    file_handler = handlers.TimedRotatingFileHandler(when=when_string,filename=FileHandler_Args[FileHandler_Args_Info.FileName],backupCount=0)
                elif FileHandler_Args[FileHandler_Args_Info.WhenAndInterval][:8].upper() =='MIDNIGHT' :
                    when_string =FileHandler_Args[FileHandler_Args_Info.WhenAndInterval][:8]
                    interval_string =FileHandler_Args[FileHandler_Args_Info.WhenAndInterval][8:]
                    file_handler = handlers.TimedRotatingFileHandler(when=when_string,filename=FileHandler_Args[FileHandler_Args_Info.FileName],interval=int(interval_string),backupCount=0)
                else:
                    when_string =FileHandler_Args[FileHandler_Args_Info.WhenAndInterval][0]
                    interval_string  = FileHandler_Args[FileHandler_Args_Info.WhenAndInterval][1:]
                    file_handler = handlers.TimedRotatingFileHandler(when=when_string,filename=FileHandler_Args[FileHandler_Args_Info.FileName],interval=int(interval_string),backupCount= 0)
                fileformat = '%(asctime)s %(levelname)-8s %(message)s'
                file_formatter = logging.Formatter(fmt=fileformat,datefmt=dateformat)
                file_handler.setFormatter(file_formatter)
                logger.addHandler(file_handler)# type: ignore
            _SetFileName_flag = True
    def __logger_is_none_exception(self):
        raise ValueError("logger is none")
    def Info(self, Message):
        if logger != None:
            logger.info(Message)
        else:
            self.__logger_is_none_exception()
    def Debug(self, Message):
        if logger != None:
            logger.debug(Message)
        else:
            self.__logger_is_none_exception()
    def Trace(self, Message):
        if logger != None:
            logger.log(TRACE, Message)
        else:
            self.__logger_is_none_exception()
    def Warn(self, Message):
        if logger != None:
            logger.warning(Message)
        else:
            self.__logger_is_none_exception()
    def Error(self, Message):
        if logger != None:
            logger.error(Message)
        else:
            self.__logger_is_none_exception()
    def Critical(self, Message):
        if logger != None:
            logger.critical(Message)
        else:
            self.__logger_is_none_exception()

if __name__ != '__main__':
    TRACE = 5
    logging.addLevelName(TRACE, 'TRACE')
    log_colors = {
        'INFO': 'white',
        'DEBUG': 'green',
        'TRACE': 'yellow',
        'WARNING': 'purple',
        'ERROR': 'red',
        'CRITICAL': 'bold_red',
    }
    secondary_log_colors = {
        'message': {
            'INFO': 'white',
            'DEBUG': 'green',
            'TRACE': 'yellow',
            'WARNING': 'purple',
            'ERROR':    'red',
            'CRITICAL': 'bold_red',
        }
    }
    
    colorformat = "%(asctime)s %(log_color)s%(levelname)-8s%(reset)s %(message_log_color)s%(message)s"
    dateformat = "%Y/%m/%d %H:%M:%S"
    stdio_formatter = colorlog.ColoredFormatter(fmt=colorformat,log_colors=log_colors,datefmt=dateformat,secondary_log_colors=secondary_log_colors)
    stdio_handler = logging.StreamHandler()
    stdio_handler.setFormatter(stdio_formatter)
    logger = logging.getLogger()
    logger.setLevel(TRACE)
    logger.addHandler(stdio_handler)
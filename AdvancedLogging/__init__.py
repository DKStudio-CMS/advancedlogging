#!/usr/bin/env python3
# coding = utf-8
# package
# __init__.py

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions
import colorlog

__all__ = [
    '__version__',
    'AdLogging'
]
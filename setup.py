import sys
import pathlib
from setuptools import setup, find_packages
import versioneer

min_version = (3, 6)

if sys.version_info < min_version:
    error = """
Advanced-Logging does not support Python {0}.{1}.
Python {2}.{3} and above is required. Check your Python version like so:

python3 --version

This may be due to an out-of-date pip. Make sure you have pip >= 9.0.1.
Upgrade pip like so:

pip install --upgrade pip
""".format(*sys.version_info[:2], *min_version)
    sys.exit(error)

here = pathlib.Path(__file__).parent.absolute()

with open(here / 'README.md', encoding='utf-8') as readme_file:
    readme = readme_file.read()

with open(here / 'requirements.txt', 'rt') as requirements_file:
    # Parse requirements.txt, ignoring any commented-out lines.
    requirements = [line for line in requirements_file.read().splitlines()
                    if not line.startswith('#')]

setup(
    name='Advanced-Logging',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    author='Jeff Tu',
    author_email='d.k.studio.cms@gmail.com',
    description='用於進階除錯Qt5轉成PyQt5,等的那種過多不明指標的程式碼，甚至用於比對測試轉換成PySide的除錯工具',
    #專案主頁
    url='https://bitbucket.org/DKStudio-CMS/advanced-logging/src/master/',
    #找尋安裝包
    packages=find_packages(),
    
    classifiers = [
        # 發展時期,常見的如下
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # 開發的目標使用者
        'Intended Audience :: Developers',

        # 屬於什麼型別
        'Topic :: Software Development :: Build Tools'

        # 許可證資訊
        'License :: OSI Approved :: LGPL License 3',

        # 目標 Python 版本
        'Programming Language :: Python :: 3'
    ],
    # 表明當前模組依賴哪些包，若環境中沒有，則會從pypi中下載安裝
    install_requires=['colorlog>=5.0.1'],

    # setup.py 本身要依賴的包，這通常是為一些setuptools的外掛準備的配置
    # 這裡列出的包，不會自動安裝。
    #setup_requires=['pbr','versioneer'],

    # 僅在測試時需要使用的依賴，在正常釋出的程式碼中是沒有用的。
    # 在執行python setup.py test時，可以自動安裝這三個庫，確保測試的正常執行。
    # tests_require=[
    #     'pytest>=3.3.1',
    #     'pytest-cov>=2.5.1',
    # ],

    # 用於安裝setup_requires或tests_require裡的軟體包
    # 這些資訊會寫入egg的 metadata 資訊中
    # dependency_links=[
    #     "http://example2.com/p/foobar-1.0.tar.gz",
    # ],

    # install_requires 在安裝模組時會自動安裝依賴包
    # 而 extras_require 不會，這裡僅表示該模組會依賴這些包
    # 但是這些包通常不會使用到，只有當你深度使用模組時，才會用到，這裡需要你手動安裝
    # extras_require={
    #     'PDF':  ["ReportLab>=1.2", "RXP"],
    #     'reST': ["docutils>=0.3"],
    # }
    python_requires='>3.6',
)
